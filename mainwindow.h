#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLegend>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QHorizontalStackedBarSeries>
#include <QtCharts/QLineSeries>
#include <QtCharts/QCategoryAxis>
#include <QtCharts/QPieSeries>
#include <QtCharts/QPieSlice>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QString m_prefixPercent = "%";

    void setInitialPopulation();
    void startEvolution();
    void setFitness(QStringList &list);
    void reproduction(QStringList list);
    void recombination(QStringList &list);
    void elitism(QStringList &list);
    void mutation(QStringList &list);

    bool checkIfPopulationContainsZero(QStringList list);

    QString generateRandomGenes();

private:
    Ui::MainWindow *ui;
    QAction *m_generatePopulation;

    int m_hromozomes;
    int m_genes;
    int m_numberOfGenerations;
    int m_elitism;
    int m_mutation;
    int m_radRepsAmmount;

    double m_maxFitness;
    double m_averageFitness;
    double m_significanceOfEachGene;
    double m_cumulativeSum;

    bool m_elitismEnabled;
    bool m_mutationEnabled;

    QStringList m_originalPopulation;
    QStringList m_newGeneration;
    QStringList m_oldGeneration;

    QList<int> m_fitness;
    QList<double> m_fitnessComulative;

    QString m_originalPopulationLabel;
    QString m_currentPopulationLabel;
    QString m_finalPopulationLabel;
    QString m_evolutionType;
    QString m_mostFitHromozom;

    void enableMutation();
    void enableElitism();
    void generatePopulation();

    QMainWindow *finishingGenerationWindow = new QMainWindow();
    QMainWindow *graphWindow = new QMainWindow();
};

#endif // MAINWINDOW_H
