#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLegend>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QHorizontalStackedBarSeries>
#include <QtCharts/QLineSeries>
#include <QtCharts/QCategoryAxis>
#include <QtCharts/QPieSeries>
#include <QtCharts/QPieSlice>

using namespace QtCharts;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->spinHromozomes->setRange(4,10);
    ui->spinHromozomes->setSingleStep(2);

    ui->spinGenes->setRange(5,8);

    ui->spinElitism->setRange(0,100);
    ui->spinElitism->setSuffix(m_prefixPercent);
    ui->spinElitism->setDisabled(true);

    ui->spinMutation->setRange(0,100);
    ui->spinMutation->setSuffix(m_prefixPercent);
    ui->spinMutation->setDisabled(true);

    connect(ui->enableMutation, &QCheckBox::stateChanged, this, &MainWindow::enableMutation);
    connect(ui->enableElitism, &QCheckBox::stateChanged, this, &MainWindow::enableElitism);

    connect(ui->generate, &QPushButton::clicked, this, &MainWindow::generatePopulation);
    connect(ui->startEvolution, &QPushButton::clicked, this, &MainWindow::startEvolution);

    ui->numberOfGenerations->setRange(10,120);
    ui->numberOfGenerations->setSingleStep(20);

    ui->startEvolution->setDisabled(true);

    ui->currentPopulation->setStyleSheet("QLabel { font-weight:bold; font-size:20pt; color : blue;margin :20px; }");
    ui->originalPopulation->setStyleSheet("QLabel { font-weight:bold; font-size:20pt; color : blue;margin :20px; }");

}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setInitialPopulation()
{
    for (int i = 0; i < m_hromozomes; ++i)
    {
        QString stringVal = generateRandomGenes();

        m_originalPopulationLabel.append(stringVal+"\n");
        m_originalPopulation.append(stringVal);
    }
}

void MainWindow::startEvolution()
{
    m_radRepsAmmount = ui->numberOfGenerations->text().toInt();
    int evolutionCounter = 0;
    m_oldGeneration.clear();
    m_newGeneration.clear();

    this->setFixedSize(750,280);
    ui->currentPopulation->setText(m_originalPopulationLabel);
    ui->generate->setDisabled(true);
    ui->startEvolution->setDisabled(true);

    m_elitism = (!m_elitismEnabled)? 0:
                  (ui->spinElitism->text().split("%")).value(0).toInt();

    m_mutation = (!m_mutationEnabled)? 0:
                  (ui->spinMutation->text().split("%")).value(0).toInt();

    QLineSeries *averageSeries = new QLineSeries();
    QLineSeries *maxSeries = new QLineSeries();
    QCategoryAxis *axisX = new QCategoryAxis();

    averageSeries->append(0, 0);
    maxSeries->append(0, 0);
    m_newGeneration = m_originalPopulation;
    setFitness(m_newGeneration);
    reproduction(m_newGeneration);

    averageSeries->append(0, m_averageFitness);
    maxSeries->append(0,m_maxFitness);

    do{
        evolutionCounter++;
        setFitness(m_newGeneration);


        averageSeries->append(evolutionCounter, m_averageFitness);
        maxSeries->append(evolutionCounter,m_maxFitness);


        reproduction(m_newGeneration);
        recombination(m_newGeneration);
        mutation(m_newGeneration);
        elitism(m_newGeneration);

        ui->currentPopulation->setText(m_currentPopulationLabel);
        m_oldGeneration = m_newGeneration;

        if (checkIfPopulationContainsZero(m_newGeneration))
        {
            evolutionCounter++;
            setFitness(m_newGeneration);
            averageSeries->append(evolutionCounter, m_averageFitness);
            maxSeries->append(evolutionCounter,m_maxFitness);
            break;
        }

    }while (evolutionCounter < m_radRepsAmmount);

    axisX->setTickCount(10);

    QChart *chart = new QChart();
    averageSeries->setName("Averge");
    maxSeries->setName("Maximum");
    chart->addSeries(averageSeries);
    chart->addSeries(maxSeries);
    chart->legend()->tr("Max");


    QFont font;
    font.setPixelSize(18);
    chart->setTitleFont(font);
    chart->setTitleBrush(QBrush(Qt::black));
    chart->setTitle("Fitness Generacija");


    QPen pen(QRgb(0xf0f000));
    pen.setWidth(5);
    averageSeries->setPen(pen);

    QPen pen1(QRgb(0x00f0f0));
    pen1.setWidth(5);
    maxSeries->setPen(pen1);

    chart->setAxisX(axisX, maxSeries);
    chart->setAxisX(axisX, averageSeries);

    chart->setAnimationOptions(QChart::AllAnimations);

    chart->createDefaultAxes();

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    graphWindow->setCentralWidget(chartView);
    graphWindow->resize(420, 300);
    graphWindow->show();

    ui->generate->setDisabled(false);
    ui->startEvolution->setDisabled(false);
}

void MainWindow::setFitness(QStringList &list)
{
    const int length = list.length();
    m_fitness.clear();
    m_fitnessComulative.clear();
    int averageFitness = 0;
    m_maxFitness = 0;
    m_cumulativeSum = 0;

    for (int i = 0; i < length; ++i)
    {
        m_fitness.append(list[i].count("1"));
        m_cumulativeSum += m_fitness[i];
        averageFitness += m_fitness[i];

        if (i == 0)
        {
            m_maxFitness = m_fitness[i];
            m_mostFitHromozom = list[i];
            continue;
        }

        const int currentFitness = m_fitness[i];

        if (m_maxFitness < currentFitness)
        {
            m_maxFitness = currentFitness;
            m_mostFitHromozom = list[i];
        }

    }


    for (int i = 0; i < length; ++i)
    {
        const double x = (1./m_cumulativeSum)*(m_fitness[i]);
        m_fitnessComulative.append(x);
    }

    m_averageFitness = ((averageFitness/m_hromozomes) * m_significanceOfEachGene);

    m_maxFitness = m_significanceOfEachGene * m_maxFitness;
}

void MainWindow::reproduction(QStringList list)
{
    const int counterLength = m_fitnessComulative.length();
    QMap<int, double> cumulative;
    m_newGeneration.clear();
    m_currentPopulationLabel.clear();

    for (int counter = 0; counter < counterLength; ++counter)
    {
        const double randomNumber = ((double) rand() / (RAND_MAX));
        cumulative.insert(0, 0);
        cumulative.insert(1, m_fitnessComulative.value(0));

        for (int counter2 = 0; counter2 < counterLength; ++counter2)
        {
            if (cumulative[0] < randomNumber && randomNumber < cumulative[1])
            {
                m_newGeneration.append(list[counter2]);
                break;
            }
            else
            {
                cumulative.insert(0, cumulative.value(1));
                cumulative.insert(1, cumulative.value(1) + m_fitnessComulative.value(counter2+1));
            }
        }

        m_currentPopulationLabel += m_newGeneration.value(counter) + "\n";

    }
    std::random_shuffle(list.begin(), list.end());
    ui->currentPopulation->setText(m_currentPopulationLabel);
}

void MainWindow::recombination(QStringList &list)
{
    for (int counter = 0; counter < m_hromozomes-1; counter += 2)
    {
        const int randNum = rand()% m_genes;
        QStringList value1 = list.value(counter).split(" ");
        QStringList value2 = list.value(counter+1).split(" ");
        list.value(counter).clear();
        list.value(counter+1).clear();


        for(int counter1 = 1; counter1 < m_genes + 1; ++counter1)
        {
            if(counter1 < randNum)
            {
                list.value(counter).append(" " + value1.value(counter1));
                list.value(counter+1).append(" " + value2.value(counter1));
            }
            else
            {
                list.value(counter+1).append(" " + value1.value(counter1));
                list.value(counter).append(" " + value2.value(counter1));
            }
        }
    }
}

void MainWindow::elitism(QStringList &list)
{
    if (m_elitismEnabled)
    {
        const double randNum = rand()%100;

        if(randNum < m_elitism)
        {
            int index = 0;
            int lowestFitness = list[0].count("1");

            for (int i = 1; i<list.length(); ++i)
            {
                if (lowestFitness < list[i].count("1"))
                {
                    lowestFitness = list[i].count("1");
                    index = i;
                }
            }

            list[index] = m_mostFitHromozom;
        }
    }
}

void MainWindow::mutation(QStringList &list)
{
    if (ui->enableMutation->isChecked())
    {
        for (int counter = 0; counter < list.length(); ++counter)
        {
            const int randNum = rand() % 100;

            if (randNum > m_mutation)
            {
                continue;
            }

            int randGene = ((rand() % m_genes)*2)-1;

            if (randGene <= 0)
            {
                randGene = 1;
            }

            QString value = list[counter];

            if (value[randGene] == "0")
            {
                value.replace(randGene,1,"1");
            }
            else
            {
                value.replace(randGene,1,"0");
            }

            list.replace(counter,value);
        }
    }
}

bool MainWindow::checkIfPopulationContainsZero(QStringList list)
{
    bool result = true;

    foreach(QString value, list)
    {
        result &= (!value.contains("0"));
    }

    return result;
}

QString MainWindow::generateRandomGenes()
{
    QString stringVal = "";

    for (int j = 0; j < m_genes; ++j)
    {
        const int numberVal = (rand()%2);
        stringVal.append(" " + QString::number(numberVal));
    }

    return stringVal;
}

void MainWindow::enableMutation()
{
    if (!ui->enableMutation->isChecked())
    {
        ui->spinMutation->setDisabled(true);
        m_mutationEnabled = false;
    }
    else
    {
        ui->spinMutation->setDisabled(false);
        m_mutationEnabled = true;
    }
}

void MainWindow::enableElitism()
{
    if (!ui->enableElitism->isChecked())
    {
        ui->spinElitism->setDisabled(true);
        m_elitismEnabled = false;
    }
    else
    {
        ui->spinElitism->setDisabled(false);
        m_elitismEnabled = true;
    }
}

void MainWindow::generatePopulation()
{
    graphWindow->hide();
    m_originalPopulation.clear();
    m_originalPopulationLabel.clear();
    m_hromozomes = ui->spinHromozomes->text().toInt();
    m_genes = ui->spinGenes->text().toInt();
    m_significanceOfEachGene = (1./m_genes);

    if (m_hromozomes % 2 == 1)
    {
        m_hromozomes-=1;
        ui->spinHromozomes->setValue(m_hromozomes);
    }

    setInitialPopulation();

    ui->startEvolution->setDisabled(false);

    ui->originalPopulation->setText(m_originalPopulationLabel);
    this->setFixedSize(500,280);


}
