# Project Title

Max One Genetic algorithm

## About the project

This project was created for Artificial Intelligence course i took in College of Applied Sciences in Subotica. For more information check [here](https://towardsdatascience.com/introduction-to-genetic-algorithms-including-example-code-e396e98d8bf3).
The Algorithm goes up to the maximum ammout of generations specified or when the Max One conditions are satisfied.

### The application is consisted of

* Menu for generating a population (You can have 8 genes and 10 hromozomes)
* Options for enabling mutation and elitism
* Ammount of maximum generations    
* A graph to display every generations fitness

### Installing

For deploying application read [this](https://doc.qt.io/qt-5/deployment.html).

## Deployment

Follow Qt guide for deploying application.Minimum version required is 5.8 .

## Built With

* [Qt](https://www.qt.io/) - C++ framework used

## Authors

* **Lazar Stipanovic** - *Developer* - [Lazo](https://gitlab.com/lazo_xD)

